package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods {

	public EditLeadPage FirstNameChange(String data){
		WebElement eleFirstNameChange  = locateElement("id", "updateLeadForm_companyName");
		type(eleFirstNameChange,data);
		return this;

	}
	public EditLeadPage clickupdate()
	{
		WebElement eleClickUpdate = locateElement("class", "decorativeSubmit");
		click(eleClickUpdate);
		return this;
	}

}
