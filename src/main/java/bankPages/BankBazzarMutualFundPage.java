package bankPages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.bankPM;

public class BankBazzarMutualFundPage extends bankPM{
	
	
	public BankBazzarMutualFundPage SearchMutualFund() {
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement search = locateElement("linktext","Search for Mutual Funds");
		click(search);
		return this;
		
	}

	public BankBazzarMutualFundPage agePAGE() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String requiredAge="26";
		WebElement drag = locateElement("xpath","//div[@class='rangeslider__handle']");
		WebElement age = locateElement("xpath","//div[@class='rangeslider__handle']/div");
		String text =age.getText();
		Actions builder = new Actions(driver);
		do {
			builder.dragAndDropBy(drag, 5, 0).build().perform();
			age= locateElement("xpath","//div[@class='rangeslider__handle']/div");
			text=age.getText();
		} while (!text.equals(requiredAge));
		return this;
		}
	public BankBazzarMutualFundPage monthPage() {
		
		WebElement month = locateElement("linktext","Dec 1991");
		click(month);
		return this;
	}
	
	public BankBazzarMutualFundPage dayofbirth() {
		
		WebElement day = locateElement("xpath","//div[@aria-label='day-14']");
	  click(day);
	  return this;
	  
	}
	
	public BankBazzarMutualFundPage ButtonContinue() {
		
		WebElement Buttoncnt = locateElement("linktext","Continue");
		click(Buttoncnt);
		return this;
	}
	
	
	public BankBazzarMutualFundPage salary() {
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String requiredSalary="4,25,000";
		WebElement drag = locateElement("xpath","//div[@class='rangeslider__handle']");
		WebElement salary = locateElement("xpath","//div[@class='rangeslider__handle-label']/span");
		String text =salary.getText();
		Actions builder = new Actions(driver);
		do {
			builder.dragAndDropBy(drag, 5, 0).build().perform();
			salary= locateElement("xpath","//div[@class='rangeslider__handle-label']/span");
			text=salary.getText();
		} while (!text.equals(requiredSalary));
		return this;
		}
		
		public BankBazzarMutualFundPage ContinueBotton() {
			WebElement cntButton = locateElement("xpath","//a[@class='btn btn-large ']");
			click(cntButton);
			return this;
		}
		
		
		public BankBazzarMutualFundPage bankselect() {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			WebElement SeleBank = locateElement("xpath","(//input[@name='primaryBankAccount'])[2]");
			click(SeleBank);
			return this;
			
		}
		
//     public BankBazzarMutualFundPage bankselectcntinue() {
//			
//			WebElement Bankcnt = locateElement("xpath","//a[@class='btn btn-large ']");
//			click(Bankcnt);
//			return this;
//     }
//		
		//a[@class='btn btn-large ']
		
		public BankBazzarMutualFundPage FnameDetail() {
			
			locateElement("xpath","//input[@placeholder='My First Name']").sendKeys("Sachin");
			return this;
			
		}
	
	public BankbazaarScheme ViewMutualFund() {
			
			WebElement ViewButton = locateElement("linktext","View Mutual Funds");
			click(ViewButton);
			return new BankbazaarScheme();
		
			
		
		
		
		//input[@name='firstName']
		}
		
	}

	
		
	
	
	
	

