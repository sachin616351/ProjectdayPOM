package bankPages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import wdMethods.bankPM;

public class BankBazzarFirstPage extends bankPM {

	public BankBazzarFirstPage mouseOverOnInvestment() {
		WebElement SelcInvest = locateElement("linktext","INVESTMENTS");
		Actions action=new Actions(driver);
		action.moveToElement(SelcInvest).build().perform();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//WebElement SelcMutual = locateElement("linktext","Mutual Funds");
		//click(SelcMutual);
		return this;
	}
	
	public BankBazzarMutualFundPage selectMutualFund() {
		 WebElement SelMutual = locateElement("linktext","Mutual Funds");
		  click(SelMutual);
		  return new BankBazzarMutualFundPage();
		//return new Bank
	}
	}
	



