/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver;
	@Given("launch the browser")
	public void launchBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 driver = new ChromeDriver();
	}

	
	@And("maximize the browser")
	public void maximize() {
		
	
		driver.manage().window().maximize();
	}
	
	@And("set the timeouts")
	 public void setTimeout() {
		 
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 }
	
	@And("enter the URL")
	public void loadURL() {
		driver.get("http://leaftaps.com/opentaps");
	}
	
	@And("enter the user name as (.*)")
	public void enterUserName(String Uname) {
		driver.findElementById("username").sendKeys(Uname);
	}
	
	@And("enter the password as (.*)")
	public void enterPassword(String Pname) {
		driver.findElementById("password").sendKeys(Pname);
	}
	
	@And("click on the login button")
	public void clickLogin() {
		
		driver.findElementByClassName("decorativeSubmit").click();
	}
	@And ("click on CRMSFA link")
	public void clickCRMSFA() {
		driver.findElementByLinkText("CRM/SFA").click();
		
	}
	
	@And ("click on leads")
	
	public void leads() {
		driver.findElementByLinkText("Leads").click();
	}
	
	
	
	
	@And("clicks on Create Lead link")
	public void clickCreatLead() {
		driver.findElementByLinkText("Create Lead").click();
	}
	
	@And ("enter the company name as (.*)")
	public void enterCName(String Cname) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(Cname);
		
	}

	
	@And ("enter the first name as (.*)")
	public void enterFName(String Fname) {
		
		driver.findElementById("createLeadForm_firstName").sendKeys(Fname);
		
	}
	
	
	@And ("enter the last name as (.*)")
	public void enterLName(String Lname) {
		
		driver.findElementById("createLeadForm_lastName").sendKeys(Lname);
		
	}
	
	@When("click on the create Lead button")
	public void clicNewkCreatLead() {
		
		driver.findElementByClassName("smallSubmit").click();
	}
	
	@Then("verify create Lead is succsess")
	public void Verify() {
		System.out.println("Success");
	}
}

*/